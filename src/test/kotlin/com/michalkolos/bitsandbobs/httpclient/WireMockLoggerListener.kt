package com.michalkolos.bitsandbobs.httpclient

import com.github.tomakehurst.wiremock.http.HttpHeaders
import com.github.tomakehurst.wiremock.http.MultiValue
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.Response
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import java.util.stream.Collectors

private const val NO_BODY_PLACEHOLDER = "[NO_BODY]"

class WireMockLoggerListener {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(WireMockLoggerListener::class.java)

        fun requestListener(request: Request, response: Response) {
            log.info("Wiremock received request: [{}] {}\n\tHeaders: \n{}\tBody: {}\n\nAnd responded with status: {}\n\tHeaders:\n{}\tBody: {}",
                request.method,
                request.absoluteUrl,
                parseHeaders(request.headers),
                parseBody(request.bodyAsString),
                parseStatusLabel(response.status),
                parseHeaders(response.headers),
                parseBody(response.bodyAsString))
        }

        private fun parseStatusLabel(code: Int) =
            try { HttpStatus.valueOf(code) }
            catch (e: IllegalArgumentException) { code }

        private fun parseBody(body: String): String =
            body.takeIf{ it.isNotBlank() }
                ?.let{ "\n" + it } ?: NO_BODY_PLACEHOLDER
        private fun parseHeaders(headers: HttpHeaders): String =
            headers.all().stream()
                .map{ convertHeadersToString(it) }
                .collect(Collectors.joining())

        private fun convertHeadersToString(headers: MultiValue) =
            "\t\t" + headers.key() + ": [ " + concatenateHeaderValues(headers.values()) + " ]\n"

        private fun concatenateHeaderValues(values: Collection<String>): String =
            values.stream()
                .collect(Collectors.joining(", ")) ?: ""
    }
}