package com.michalkolos.bitsandbobs.httpclient

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.reactive.*
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import reactor.util.retry.Retry
import java.time.Duration
import kotlin.test.assertTrue


internal const val CONTENT_TYPE_HEADER_NAME = "content-type"
internal const val CONTENT_TYPE_HEADER_VALUE = "application/json; charset=utf-8"

private const val ENDPOINT = "/api/test"

private const val RESPONSE_BODY = """{"response": "You are awesome!"}"""
private const val BASE_URL = "http://localhost:"

@OptIn(ExperimentalCoroutinesApi::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = [WireMockInitializer::class])
class HttpClientTests(@Autowired val server: WireMockServer) {

    @Test
    fun clientConnectsToWebService() {
        stubEndpoint(ENDPOINT, HttpStatus.ACCEPTED.value(), RESPONSE_BODY)

        runTest {
            val response = AwesomeHttpClientProducer()
                .getClient(BASE_URL + server.port())
                .method(HttpMethod.GET)
                .uri{it
                    .path(ENDPOINT).build()}
                .retrieve()
                .toEntity(String::class.java)
                .retryWhen(Retry.fixedDelay(10, Duration.ofSeconds(1)))
                .awaitFirst()


            assertTrue { response.statusCode == HttpStatus.ACCEPTED }
            assertTrue { response.body == RESPONSE_BODY }
        }

    }


    fun stubEndpoint(path: String, responseStatus: Int, responseBody: String) {

        server.stubFor(
            WireMock.get(WireMock.urlEqualTo(path))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(responseStatus)
                        .withHeader(CONTENT_TYPE_HEADER_NAME, CONTENT_TYPE_HEADER_VALUE)
                        .withBody(responseBody)
                )
        )
    }
}
