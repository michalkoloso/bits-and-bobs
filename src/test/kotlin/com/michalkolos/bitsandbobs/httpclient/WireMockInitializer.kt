package com.michalkolos.bitsandbobs.httpclient

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.ContextClosedEvent
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.context.ApplicationContextInitializer

private const val WIREMOCK_BEAN_NAME = "wireMockServer"



class WireMockInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    private val log: Logger = LoggerFactory.getLogger(WireMockInitializer::class.java)
    private val mapper = ObjectMapper()

    init {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
    }

    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        WireMockConfiguration.wireMockConfig().httpDisabled(false)
        WireMockConfiguration.wireMockConfig().stubCorsEnabled(true)

        val options = WireMockConfiguration.wireMockConfig()
            .httpDisabled(false)
            .stubCorsEnabled(true)
            .dynamicPort()


        val wireMockServer = WireMockServer(options)
        wireMockServer.addMockServiceRequestListener(WireMockLoggerListener::requestListener)

        wireMockServer.start()
        log.info("Started WireMock server with settings: {}{}",
            System.lineSeparator(),
            mapper.writerWithDefaultPrettyPrinter().writeValueAsString(options))

        //  Stoping wiremock when application context closes
        applicationContext.addApplicationListener {
            if (it is ContextClosedEvent) {
                wireMockServer.stop()
            }
        }

        //  Registering wiremock server bean in the context
        applicationContext.beanFactory
            .registerSingleton(WIREMOCK_BEAN_NAME, wireMockServer)

//        Replacing url setting with wiremock url (optional)
//        val wiremockUrl = wireMockServer.baseUrl()
//        TestPropertyValues
//            .of(mapOf(API_ENDPOINT_SETTING to wiremockUrl))
//            .applyTo(applicationContext)
//
//        log.info("Setting \"$API_ENDPOINT_SETTING\" to \"$wiremockUrl\"")
    }


}