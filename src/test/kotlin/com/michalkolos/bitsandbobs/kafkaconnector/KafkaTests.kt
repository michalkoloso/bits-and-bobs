package com.michalkolos.bitsandbobs.kafkaconnector

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.test.runTest
import org.apache.kafka.common.serialization.StringDeserializer
import org.junit.Before
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.test.EmbeddedKafkaBroker
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.test.annotation.DirtiesContext
import org.apache.kafka.common.serialization.StringSerializer
import kotlin.test.assertEquals

private const val TEST_MESSAGE_KEY = "key"
private const val TEST_MESSAGE_BODY = "testMessage"
private const val TEST_TOPIC = "test-topic"
private const val PRODUCER_ID = "fancy-test-producer"
private const val CONSUMER_ID = "fancy-test-consumer"
private const val CONSUMER_GROUP_ID = "sample-group"

@OptIn(ExperimentalCoroutinesApi::class)
@SpringBootTest
@DirtiesContext
@EmbeddedKafka
class KafkaTests(
    @Autowired private val embeddedKafkaBroker: EmbeddedKafkaBroker) {

    companion object {
        val log: Logger = LoggerFactory.getLogger(KafkaTests::class.java)
    }

    @Before
    fun init() {
        System.setProperty("spring.kafka.bootstrap-servers", embeddedKafkaBroker.brokersAsString)
        log.info("Instantiated embedded Kafka broker: ${embeddedKafkaBroker.brokersAsString}")
    }

    @Test
    @DirtiesContext
    fun producerSendsMessageAndConsumerReceivesIt() = runTest {
        val producer = AwesomeKafkaProducer<String, String>(embeddedKafkaBroker.brokersAsString, PRODUCER_ID,
            StringSerializer::class.java, StringSerializer::class.java)
        val consumer = AwesomeKafkaConsumer<String, String>(embeddedKafkaBroker.brokersAsString, CONSUMER_ID, CONSUMER_GROUP_ID,
            StringDeserializer::class.java, StringDeserializer::class.java)

        val receivedMessageFlow = consumer.subscribeToTopic(TEST_TOPIC)
        val payloadFlow = flowOf(Pair(TEST_MESSAGE_KEY, TEST_MESSAGE_BODY))
        val sentMessage = producer.sendFlow(TEST_TOPIC, payloadFlow)
            .first()


        val receivedMessage = receivedMessageFlow.first()

        assertEquals(receivedMessage.value(), TEST_MESSAGE_BODY)
        assertEquals(sentMessage.second.exception(), null)
    }
}