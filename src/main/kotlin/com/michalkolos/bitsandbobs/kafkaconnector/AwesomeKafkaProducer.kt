package com.michalkolos.bitsandbobs.kafkaconnector

import kotlinx.coroutines.flow.*
import kotlinx.coroutines.reactive.*
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.kafka.sender.KafkaSender
import reactor.kafka.sender.SenderOptions
import reactor.kafka.sender.SenderRecord
import reactor.kafka.sender.SenderResult
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.UUID

private const val DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd hh:mm:ss"

class AwesomeKafkaProducer<K, V>(
    private val bootstrapServer: String,
    private val producerId: String,
    private val keySerializer: Class<*>,
    private val valueSerializer: Class<*>) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(AwesomeKafkaProducer::class.java)
    }

    private var sender: KafkaSender<K, V> = createSender()

    private val dateFormat: DateTimeFormatter =
        DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN).withZone(ZoneId.of("UTC"))


    private fun createSender() =
        KafkaSender.create(getSenderOptions(bootstrapServer, producerId, keySerializer, valueSerializer))
            ?.also { log.info("Created Kafka sender on server: '$bootstrapServer' with id: '$producerId'") }
            ?: throw AwesomeKafkaProducerException(
                        "Unable to create Kafka sender on server: '$bootstrapServer' with id: '$producerId'")



    fun sendFlow(topic: String, messageFlow: Flow<Pair<K, V>>) =
        messageFlow
            .map { buildRecord(topic, it) }
            .let { sendRecords(it, sender) }
            .onEach{ logSendingSuccess(it.second) }
            .catch { logSendingFailure(it, topic) }
            .onCompletion { logFinish(it, topic) }


    private fun getSenderOptions(server: String, senderId: String, keySerializer: Class<*>, valueSerializer: Class<*>): SenderOptions<K, V> =
        SenderOptions.create(
            mapOf<String, Any>(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG to server,
                ProducerConfig.CLIENT_ID_CONFIG to senderId,
                ProducerConfig.ACKS_CONFIG to "all",
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG to keySerializer,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG to valueSerializer
            )
        )


    private fun buildRecord(topic: String, message: Pair<K, V>): SenderRecord<K, V, String> = message
        .let { ProducerRecord(topic, it.first, it.second) }
        .let { SenderRecord.create(it, UUID.randomUUID().toString()) }

    private fun sendRecords(record: Flow<SenderRecord<K, V, String>>, producer: KafkaSender<K, V>) =
        record
            .asPublisher()
            .let(producer::send)
            .asFlow()
            .map { Pair(record, it) }

    private fun logSendingSuccess(result: SenderResult<String>) = with(result) {
        val formattedTimestamp = recordMetadata()?.timestamp()
            ?.let(Instant::ofEpochMilli)
            ?.let(dateFormat::format)
            ?: "MISSING"

        log.info(
            "Sent message: " +
                    "topic-partition: '${recordMetadata()?.topic() ?: "MISSING"}-${recordMetadata()?.partition() ?: "MISSING"}', " +
                    "timestamp:'$formattedTimestamp/, " +
                    "key bytes: '${recordMetadata()?.serializedKeySize() ?: "MISSING"}', " +
                    "value bytes: '${recordMetadata()?.serializedValueSize() ?: "MISSING"}'"
        )
    }

    private fun logFinish(cause: Throwable?, topic: String) {
        val causeMessage = cause
            ?.let { "due to: ${it.message}" }
            ?: ""

        log.info("Finished sending flow on topic: $topic $causeMessage")
    }

    private fun logSendingFailure(error: Throwable, topic: String) {
        log.error("Error sending message on topic $topic: ${error.message}", error.cause)
    }

    private fun runCleanup(cause: Throwable?, topic: String, producer: KafkaSender<K, V>) {
        val causeMessage = cause
            ?.let { "due to: ${it.message}" }
            ?: ""

        log.info("Closing Kafka producer for topic: $topic $causeMessage")
        producer.close()
    }

    class AwesomeKafkaProducerException: RuntimeException {
        constructor(message: String) : super(message)
        constructor(message: String, cause: Throwable) : super("$message, reason: ${cause.message}", cause)
    }
}