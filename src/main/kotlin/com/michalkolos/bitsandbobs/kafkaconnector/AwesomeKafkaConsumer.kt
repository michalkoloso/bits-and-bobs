package com.michalkolos.bitsandbobs.kafkaconnector

import kotlinx.coroutines.flow.*
import kotlinx.coroutines.reactive.asFlow
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.kafka.receiver.KafkaReceiver
import reactor.kafka.receiver.ReceiverOptions
import reactor.kafka.receiver.ReceiverRecord
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter


private const val DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd hh:mm:ss"
private const val OFFSET_RESET_SETTING = "earliest"


class AwesomeKafkaConsumer<K, V>(
    private val bootstrapServer: String,
    private val consumerId: String,
    private val groupId: String,
    private val keyDeserializer: Class<*>,
    private val valueDeserializer: Class<*>) {

    private val dateFormat: DateTimeFormatter =
        DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN).withZone(ZoneId.of("UTC"))

    fun subscribeToTopic(topic: String) =
        KafkaReceiver.create(getReceiverOptions(topic))
            .receive()
            .asFlow()
            .onEach(this::logIncomingMessage)
            .catch { logReceivingFailure(it, topic) }
            .onCompletion { logFinish(it, topic) }


    private fun getReceiverOptions(topic: String) =
        ReceiverOptions.create<K, V>(
            mapOf<String, Any>(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServer,
                ConsumerConfig.CLIENT_ID_CONFIG to consumerId,
                ConsumerConfig.GROUP_ID_CONFIG to groupId,
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG to keyDeserializer,
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG to valueDeserializer,
                ConsumerConfig.AUTO_OFFSET_RESET_CONFIG to OFFSET_RESET_SETTING))
            .subscription(setOf(topic))
            .addAssignListener{ log.info("Assigned partitions: $it")}
            .addRevokeListener{ log.info("Revoked partitions: $it")}

    private fun logIncomingMessage(record: ReceiverRecord<*,*>) {
        val consumerRecord: ConsumerRecord<*, *> = record

        val formattedTimestamp = record.timestamp()
                .let(Instant::ofEpochMilli)
                .let(dateFormat::format)
                ?:"MISSING"

       log.info("Received message: " +
                "topic-partition: '${record.receiverOffset()?.topicPartition()?:"MISSING"}', " +
                "offset: '${record.receiverOffset()?.offset()?:"MISSING"}', " +
                "timestamp: '$formattedTimestamp', " +
               "key bytes: '${consumerRecord.serializedKeySize()}', " +
               "value bytes: '${consumerRecord.serializedValueSize()}'")
    }

    private fun logFinish(cause: Throwable?, topic: String) {
        val causeMessage = cause
            ?.let { "due to: ${it.message}" }
            ?: ""

        log.info("Finished receiving Flow on topic: $topic $causeMessage")
    }

    private fun logReceivingFailure(error: Throwable, topic: String) {
        log.error("Error receiving message on topic $topic: ${error.message}", error.cause)
    }

    companion object{
        private val log: Logger = LoggerFactory.getLogger(AwesomeKafkaConsumer::class.java)
    }
}