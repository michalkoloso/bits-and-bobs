package com.michalkolos.bitsandbobs.httpclient

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.ClientRequest
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeFilterFunction.ofRequestProcessor
import org.springframework.web.reactive.function.client.ExchangeFilterFunction.ofResponseProcessor
import org.springframework.web.reactive.function.client.ExchangeFunction
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

class AwesomeHttpClientProducer {

    private val log: Logger = LoggerFactory.getLogger(WebClient::class.java)
    fun getClient(baseUrl: String) =
        WebClient.builder()
            .filters {
//                it.add(logRequest())
//                it.add(logResponse())
                it.add(logBoth())}
            .baseUrl(baseUrl)
            .build()

    private fun logRequest(): ExchangeFilterFunction {
        return ofRequestProcessor {

            log.info(requestAsString(it))
            Mono.just(it)
        }
    }

    private fun logResponse(): ExchangeFilterFunction {
        return ofResponseProcessor {
            val stringBuilder = StringBuilder()
            stringBuilder.append(it.statusCode().toString())
            log.info(stringBuilder.toString())
            Mono.just(it)
        }
    }

    private fun logBoth(): ExchangeFilterFunction {
        return ExchangeFilterFunction { request: ClientRequest, next: ExchangeFunction ->
            log.info(requestAsString(request))

            next.exchange(request).doOnNext{response: ClientResponse ->
                log.info(responseAsString(request, response))
            }
        }
    }

    private fun requestAsString(request: ClientRequest): String {
        val stringBuilder = StringBuilder("Calling: ")
        stringBuilder.append(request.method().name()).append(" ")
        stringBuilder.append(request.url().toString()).append(" [")

        request.headers()
            .forEach { name, values ->
                stringBuilder.append(name).append(": ")
                values.forEach{ value -> stringBuilder.append(value).append(", ")}
                stringBuilder.append(System.lineSeparator())
            }
        stringBuilder.append("]")

        return stringBuilder.toString()
    }

    private fun responseAsString(request: ClientRequest, response: ClientResponse): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(request.url().toString()).append(" responded with: ")
        stringBuilder.append(response.statusCode().toString())

        return stringBuilder.toString()
    }
}