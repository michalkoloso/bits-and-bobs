import sys
import re

BUILD_GRADLE_FILENAME = 'build.gradle.kts'
README_FILENAME = 'readme.md'
VERSION_PREFIX = 'version'


def main():
    level = 1
    if len(sys.argv) > 1:
        level = int(sys.argv[1])

    version_tag = update_build_gradle(level)
    update_readme(version_tag)
    print(version_tag)


def update_build_gradle(level):
    file_lines = read_file_lines(BUILD_GRADLE_FILENAME)
    index = get_token_line_index(file_lines, VERSION_PREFIX)
    version_line = file_lines[index]
    version_tag = re.findall(r'[\',\"](.*?)[\',\"]', version_line)[0]
    version_tag = increment_version(version_tag, level)
    file_lines[index] = VERSION_PREFIX + ' = "' + version_tag + '"'
    write_file_lines(file_lines, BUILD_GRADLE_FILENAME)

    return version_tag


def update_readme(version_tag):
    file_lines = read_file_lines(README_FILENAME)
    index = get_token_line_index(file_lines, VERSION_PREFIX)
    file_lines[index] = VERSION_PREFIX + ' ' + version_tag
    write_file_lines(file_lines, README_FILENAME)


def get_token_line_index(lines, token):
    token_line_index = -1

    for i, line in enumerate(lines):
        if line.startswith(token):
            token_line_index = i

    return token_line_index


def write_file_lines(lines, filename):
    with open(filename, 'w') as readme_file:
        readme_file.write("\n".join(lines))


def read_file_lines(filename):
    lines = []

    with open(filename, 'r') as readme_file:
        lines = readme_file.read().split("\n")

    return lines


def increment_version(version_tag, level):
    version_numbers = version_tag.split(".")

    for i, version_number in enumerate(version_numbers):
        version_number = int(version_number)

        if i == level:
            version_number = version_number + 1

        if i > level:
            version_number = 0

        version_numbers[i] = str(version_number)

    return ".".join(version_numbers)


if __name__ == "__main__":
    main()
